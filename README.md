<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# F5 Pool Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview

This Pre-Built uses the [Ansible Big IP Pool](https://docs.ansible.com/ansible/latest/collections/f5networks/f5_modules/bigip_pool_module.html) module to create an F5 pool.

## Operations Manager and JSON-Form

This workflow has an [Operations Manager item](./bundles/ac_agenda_jobs/F5%20Pool%20Creation.json) that calls a workflow. The Operations Manager item uses a JSON-Form to specify common fields populated when a pool is created. The workflow the Operations Manager item calls queries data from the formData job variable.

<table>
  <tr>
    <td>
      <img src="./images/f5_pool_creation_ac_form.png" alt="form" width="800px">
    </td>
  </tr>
  <tr>
    <td>
      <img src="./images/f5_pool_creation_canvas.png" alt="form" width="800px">
    </td>
  </tr>
</table>


## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2023.1`
* Itential Automation Gateway
  * `2023.1`

## Requirements

This Pre-Built requires the following:

* IAG Adapter Configured
* F5 BigIP Load Balancer on-boarded in Ansible

## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows user to create a pool leveraging simple form
* Leverages F5 Ansible modules

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Automation Catalog item `F5 Pool Creation` or call [F5 Pool Management](./bundles/workflows/F5%20Pool%20Management.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "device": "Target F5 host",
  "name": "Pool name",
  "description": "Descriptive text that identifies the pool",
  "loadBalancerMethodType": "Load balancing method",
  "state": "The pool state (present or absent)"
}
```


## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
